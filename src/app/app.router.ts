import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes: Route[] = [
  { loadChildren: 'app/characterDetail/characterDetail.module#characterDetailModule', path: 'characterDetail' },
  { loadChildren: 'app/dashboard/dashboard.module#DashboardModule', path: 'dashboard' },
  { path: '', pathMatch: 'full', redirectTo: '/dashboard' }

];

export const routing: ModuleWithProviders = RouterModule.forRoot(
  routes,
  {
    useHash: true
  }
);
