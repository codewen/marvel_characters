import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class CharacterService {

  public currentDisplayCharacter;

  constructor(private http: Http) {


  }

  /**
   * Air Quality Index
   *
   * @param longitude
   * @param latitude
   * @returns {Observable<Response>}
   */
  getCharacters(limit: number, offset: number, orderBy?: String, nameStartsWith?: String): Observable<{}> {

    let nameStartsWithString = "";
    if (!orderBy) {
      orderBy = "name"
    }
    if (nameStartsWith) {
      nameStartsWithString = "&nameStartsWith=" + nameStartsWith;
    }

    return this.http.get(`http://localhost:4300/api/public/simple?limit=${limit}&offset=${offset}&orderBy=${orderBy}`
      + nameStartsWithString);
  }

  setDisplayCharacter(character: any) {
    this.currentDisplayCharacter = character;
  }
  getDisplayCharacter() {
    return this.currentDisplayCharacter;
  }

  getCharacterDetail(resourceURI) {
    return this.http.get('http://localhost:4300/api/public/detail?resourceURI=' + encodeURI(resourceURI));
  }

}
