import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { routing } from './app.router';
import { effects, store, instrumentation } from './store';
import { SharedModule } from './shared/shared.module';
import { CharacterService } from './service/character.service';
import { WeatherService } from './weather/weather.service';
import { characterDetailModule } from './characterDetail/characterDetail.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    FormsModule,
    characterDetailModule,
    HttpModule,
    store,
    effects,
    routing,
    instrumentation
  ],
  providers: [
    WeatherService,
    CharacterService,
    
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
