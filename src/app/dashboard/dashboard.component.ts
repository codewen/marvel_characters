import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CharacterService } from '../service/character.service';
import { Http, Response } from '@angular/http';



import { FEED_ADD, FEED_REMOVE, FEED_ADD_COMMENT } from '../store/feed/feed.actions';
import { IAppState } from '../store';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  public charactersList = [];
  public nothingIsFound: boolean = false;
  public loading: boolean = false;
  public sortBy: String = 'name';
  public nameStartsWith: String = '';


  public currentPage: number = 1;
  public itemPerPage: number = 50;
  public total: number = 0;
  public totalPage: number = 0;

  constructor(private characterService: CharacterService) {

  }

  ngOnInit() {
    this.loading = true;
    this.characterService.getCharacters(this.itemPerPage, 0, this.sortBy, this.nameStartsWith)
      .map((response: Response) => response.json())
      .subscribe((res) => {
        let resJSON = JSON.parse(res);
        this.updateCharacterList(resJSON);

      });
  }

  updateCharacterList(resJSON) {
    this.charactersList = resJSON.data.results
    this.loading = false;
    this.total = resJSON.data.total;
    this.totalPage = Math.ceil(this.total / this.itemPerPage);
  }

  goToPage() {
    if (Number.isInteger(this.currentPage) && this.currentPage <= this.totalPage && this.currentPage >= 1) {
      this.loading = true;
      this.characterService.getCharacters(this.itemPerPage, (this.currentPage - 1) * this.itemPerPage, this.sortBy, this.nameStartsWith)
        .map((response: Response) => response.json())
        .subscribe((res) => {
          let resJSON = JSON.parse(res);
          this.updateCharacterList(resJSON);
        });
    }

  }

  searchCharacterList() {
    if (this.nameStartsWith.length > 0) {
      this.charactersList = [];
      this.nothingIsFound = false;
      this.currentPage = 0;
      this.total = 0;
      this.totalPage = 0;
      this.loading = true;
      this.characterService.getCharacters(this.itemPerPage, 0, this.sortBy, this.nameStartsWith)
        .map((response: Response) => response.json())
        .subscribe((res) => {
          let resJSON = JSON.parse(res);
          this.updateCharacterList(resJSON);
          this.loading = false;

          if (this.total === 0) {
            //nothing is found
            this.nothingIsFound = true;
          }

        });
    }

  }

  reSortBy() {
    this.loading = true;
    this.currentPage = 1;
    this.characterService.getCharacters(this.itemPerPage, 0, this.sortBy, this.nameStartsWith)
      .map((response: Response) => response.json())
      .subscribe((res) => {
        this.charactersList = JSON.parse(res).data.results
        this.loading = false;
      });
  }

  setDisplayCharacter(char){
    this.characterService.setDisplayCharacter(char);
  }

}
