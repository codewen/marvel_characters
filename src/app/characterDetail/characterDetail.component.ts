import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Http, Response } from '@angular/http';


import { CharacterService } from '../service/character.service';



@Component({
  selector: 'character-detail',
  styleUrls: ['./characterDetail.component.css'],
  templateUrl: './characterDetail.component.html'
})

export class characterDetailComponent {
  public character: any;
  public loading: boolean;

  constructor(private characterService: CharacterService) {

  }
  ngOnInit() {
    this.loading = true;
    this.characterService.getCharacterDetail(this.characterService.getDisplayCharacter().resourceURI)
      .map((response: Response) => response.json())
      .subscribe((res) => {
        let resJSON = JSON.parse(res);
        this.character = resJSON.data.results[0];
        this.loading = false;

      });;
  }
}
