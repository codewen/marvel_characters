import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { characterDetailComponent } from './characterDetail.component';
import { routing } from './characterDetail.router';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    characterDetailComponent,
  ],
  bootstrap: [
    characterDetailComponent
  ]
})
export class characterDetailModule {}
