import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { characterDetailComponent } from './characterDetail.component';

const routes: Route[] = [
  {
    path: 'characterDetail',
    component: characterDetailComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
