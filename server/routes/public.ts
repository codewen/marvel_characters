import { Router, Response, Request } from 'express';

const publicRouter: Router = Router();

var http = require("http");
var https = require("https");
var request = require('request');


publicRouter.use('/simple', (theRequest: Request, theResponse: Response) => {

  theResponse.header('Access-Control-Allow-Origin', '*');
  theResponse.header('Access-Control-Allow-Headers', 'X-XSRF-TOKEN');
  request(
    {
      method: 'GET',
      uri: 'http://gateway.marvel.com/v1/public/characters',
      qs: {
        apikey: '6e1726c3cf816facf4284b20c45ff28f',
        hash: 'ee88c100ea8aac8219efefd899b027d4',
        ts: '1',
        limit: theRequest.query.limit,
        offset: theRequest.query.offset,
        orderBy: theRequest.query.orderBy,
        nameStartsWith: theRequest.query.nameStartsWith
      }

    },
    function (error, response, body) {
      theResponse.json(body);
    });
});

publicRouter.use('/detail', (theRequest: Request, theResponse: Response) => {

  theResponse.header('Access-Control-Allow-Origin', '*');
  theResponse.header('Access-Control-Allow-Headers', 'X-XSRF-TOKEN');
  request(
    {
      method: 'GET',
      uri: decodeURI(theRequest.query.resourceURI),
      qs: {
        apikey: '6e1726c3cf816facf4284b20c45ff28f',
        hash: 'ee88c100ea8aac8219efefd899b027d4',
        ts: '1'
      }

    },
    function (error, response, body) {
      theResponse.json(body);
    });
});

export { publicRouter }
